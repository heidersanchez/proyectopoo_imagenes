import numpy as np
from PIL import Image
from Modulos import *
from os import listdir
from os.path import isfile, join

####### FUNCIONES - CONSOLA ############
def leerImagen(file_in):
	image = Image.open(file_in)
	im_np = np.asarray(image)
	return im_np.tolist()

def guardarImagen(ma_ouput, file_out):
	im_np = np.array(ma_ouput, dtype=np.uint8)
	image = Image.fromarray(im_np)
	image.save(file_out)

def leerHistograma(file_in):
	image = Image.open(file_in)
	# obtener el histograma de colores de la imagen 
	histograma = image.histogram()
	return histograma

def leerColeccion(folder):
	files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]
	colection = {}
	for	file_image in files:
		colection[file_image] = leerHistograma(file_image)
	return colection
	

####### PROGRAMA PRINCIPAL - PROYECTO2 ############
## El nombre de la foto debe ser un dato de entrada
matriz_img = leerImagen("image_input.png")
## Debe implementar un menu de opciones 
# Probar: voltear vertical
matriz_img = voltearVertical(matriz_img)
guardarImagen(matriz_img, 'image_output_vertical.png')
# Probar: girar 90 grados
matriz_img = girar90(matriz_img)
guardarImagen(matriz_img, 'image_output_90.png')


####### PROGRAMA PRINCIPAL - PROYECTO3 ############
query = leerHistograma("coleccion/leon1.jpg")
coleccion = leerColeccion("coleccion/")
resultado = buscarSimilares(query, coleccion, 3)
print(resultado)




