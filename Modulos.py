#############################################################
#####                PROYECTO  2                        #####
#############################################################
def girar90Antihorario(ma_input):
    #matriz ma_input es n x m
    n = len(ma_input)
    m = len(ma_input[0])
    #inicializar ma_output m x n con ceros
    ma_output =[[0 for j in range(n)]
                   for i in range(m)]
    #copiar elementos
    for i in range(n):
        for j in range(m):
            ma_output[m - j - 1][i] = ma_input[i][j]
    return ma_output

def girar90Horario(ma_input):
    #matriz ma_input es n x m
    n = len(ma_input)
    m = len(ma_input[0])
    #inicializar ma_output m x n con ceros
    ma_output =[[0 for j in range(n)]
                   for i in range(m)]
    #copiar elementos
    for i in range(n):
        for j in range(m):
            ma_output[j][n- i - 1] = ma_input[i][j]
    return ma_output

def reflejarHorizontal(ma_input):
    #matriz ma_input es n x m
    n = len(ma_input)
    m = len(ma_input[0])
    #inicializar ma_output n x m con ceros
    ma_output =[[0 for j in range(m)]
                   for i in range(n)]
    #copiar elementos
    for i in range(n):
        for j in range(m):
            ma_output[i][m - j - 1] = ma_input[i][j]
    return ma_output

def reflejarVertical(ma_input):
    #matriz ma_input es n x m
    n = len(ma_input)
    m = len(ma_input[0])
    #inicializar ma_output n x m con ceros
    ma_output =[[0 for j in range(m)]
                   for i in range(n)]
    #copiar elementos
    for i in range(n):
        for j in range(m):
            ma_output[n - i - 1][j] = ma_input[i][j]
    return ma_output
    
# Funcion para hacer zoom
def zoom(ma_input, factor):
    # implemente aqui
    
    return

#############################################################
#####               PROYECTO 3                          #####
#############################################################
# Funcion para medir la distancia entre dos histogramas
def distancia(P, Q):
    #--- implemente aqui-----
    suma = 0
    maxp=max(P)
    maxq=max(Q)
    for i in range(len(P)):
        suma += abs(P[i]/maxp - Q[i]/maxq)    
    return suma

# Funcion para buscar las k imagenes mas similares al query
# query: histograma,  coleccion = {"imagen1": histograma1, "imagen2": histograma2, ...}
def buscarSimilares(query, coleccion, k): 
    resultado = []
    # 1- calcular la distancia entre el query y cada imagen
    #--- implemente aqui-----
    for imagen, histograma  in coleccion.items():
        dist = distancia(query, histograma)    
        resultado.append((imagen, dist))
        
    # 2- ordenar la lista resultado
    #--- implemente aqui-----    
    for i in range(0, len(resultado) - 1):
        for j in range(i, len(resultado)):
            if resultado[i][1] > resultado[j][1]:
                aux = resultado[i]
                resultado[i] = resultado[j]
                resultado[j] = aux
                    
    # 3- retorna los k primeros elementos    
    return resultado[:k]  




