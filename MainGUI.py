import numpy as np
from PIL import Image, ImageTk
from tkinter import Tk, Menu, Toplevel
from tkinter.ttk import Frame, Label, Button
import sys
from os import listdir
from os.path import isfile, join
from Modulos import *

class GUIImagen(Frame):
    def __init__(self):
        super().__init__()
        self.k = 4 # imagenes de la coleccion a mostrar
        self.leerImagen()
        self.iniciarGUI()

    def leerImagen(self):
        try:
            self.image = Image.open("coleccion/perro1.jpg")            
        except IOError:
            print("No se pudo leer la imagen")
            sys.exit(1)
    
    def iniciarGUI(self):
        self.master.title("VisorImagen")
        self.panelImage = Label(self)
        self.panelImage.pack(side="top")
        self.panelColection = Label(self) 
        self.panelColection.pack(side="bottom")
        self.panelImages = [0 for i in range(self.k)]
        for i in range(self.k):
            self.panelImages[i] = Label(self.panelColection)
            self.panelImages[i].grid(column=i, row=0)
        self.pack()
        self.ponerImagen()

    def ponerImagen(self):
        tatras = ImageTk.PhotoImage(self.image)
        self.panelImage.configure(image=tatras)
        self.panelImage.image = tatras    

    def posicionar(self):
        w, h = self.image.size
        self.master.geometry(("%dx%d+300+300") % (w, h+150))

    def btnReflejoVertical(self):
        ma_input = np.asarray(self.image).tolist()
        ma_ouput = reflejarVertical(ma_input)
        self.image = Image.fromarray(np.array(ma_ouput, dtype=np.uint8))  
        self.ponerImagen()

    def btnReflejoHorizontal(self):
        ma_input = np.asarray(self.image).tolist()
        ma_ouput = reflejarHorizontal(ma_input)
        self.image = Image.fromarray(np.array(ma_ouput, dtype=np.uint8))  
        self.ponerImagen()
    
    def btnGirar90Horario(self):
        ma_input = np.asarray(self.image).tolist()
        ma_ouput = girar90Horario(ma_input)
        self.image = Image.fromarray(np.array(ma_ouput, dtype=np.uint8))  
        self.ponerImagen()

    def btnGirar90Antihorario(self):
        ma_input = np.asarray(self.image).tolist()
        ma_ouput = girar90Antihorario(ma_input)
        self.image = Image.fromarray(np.array(ma_ouput, dtype=np.uint8))  
        self.ponerImagen()
    
    def btnBuscar(self):
        folder = "coleccion/"
        files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]
        coleccion = {}
        for	file_image in files:
        	imageC = Image.open(file_image)
        	coleccion[file_image] = imageC.histogram()

        query = self.image.histogram()
        resultado = buscarSimilares(query, coleccion, self.k)
        print(resultado)
        i = 0
        for (file_image, dist) in resultado:
    	    imageC = Image.open(file_image)
    	    imageC = imageC.resize((150, 150), Image.ANTIALIAS)
    	    tatras = ImageTk.PhotoImage(imageC)
    	    self.panelImages[i].configure(image=tatras)
    	    self.panelImages[i].image = tatras  
    	    i += 1
        
                     
####### PROGRAMA PRINCIPAL - GUI ############
root = Tk()
gimagen = GUIImagen()
gimagen.posicionar()
# crear menu 
menubar = Menu(root)
menubar.add_command(label="Girar -90°", command=gimagen.btnGirar90Antihorario)
menubar.add_command(label="Girar +90°", command=gimagen.btnGirar90Horario)
menubar.add_command(label="Reflejo Vertical", command=gimagen.btnReflejoVertical)
menubar.add_command(label="Reflejo Horizontal", command=gimagen.btnReflejoHorizontal)
menubar.add_command(label="Buscar", command=gimagen.btnBuscar)
menubar.add_command(label="Salir!", command=root.quit)
# mostrar el menu
root.config(menu=menubar)
# correr
root.mainloop()

